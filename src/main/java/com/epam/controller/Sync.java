package com.epam.controller;

public class Sync {
    private final Object sync = new Object();

    public void run() {
        new Thread(this::printA).start();
        new Thread(this::printB).start();
        new Thread(this::printC).start();
    }

    public void printA(){
        synchronized (sync){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("A");
        }
    }

    public void printB(){
        synchronized (sync){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("B");
        }
    }

    public void printC(){
        synchronized (sync){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("C");
        }
    }
}
