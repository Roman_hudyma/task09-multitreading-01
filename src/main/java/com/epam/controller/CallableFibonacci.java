package com.epam.controller;


import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class CallableFibonacci {
    public void run() {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        try {
            long sumFibonacci = executor.invokeAll(generateCallable()).stream()
                    .mapToLong(this::getValue)
                    .sum();
            System.out.println(sumFibonacci);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executor.shutdown();
    }

    private long getValue(Future<Long> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    private List<Callable<Long>> generateCallable() {
        final int[] array = {5, 2, 21, 14, 12, 15};
        return Arrays.stream(array)
                .mapToObj(value -> (Callable<Long>) () ->find(value))
                .collect(Collectors.toList());
    }

    public long find(int n) {
        if (n <= 1) {
            return n;
        }
        return find(n - 1) + find(n - 2);
    }
}
