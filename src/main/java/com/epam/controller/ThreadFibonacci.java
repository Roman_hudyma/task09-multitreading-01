package com.epam.controller;

public class ThreadFibonacci {
    public void run() {
        final int[] array = {2, 1, 13, 4, 12, 56};
        for (final int value : array) {
            new Thread(() -> System.out.println(new CallableFibonacci().find(value))).start();
        }
    }
}
