package com.epam.controller;

public class MultiSync {
    private final Object syncA = new Object();
    private final Object syncB = new Object();
    private final Object syncC = new Object();

    public void run() {
        new Thread(this::printA).start();
        new Thread(this::printB).start();
        new Thread(this::printC).start();
    }

    public void printA(){
        synchronized (syncA){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("A");
        }
    }

    public void printB(){
        synchronized (syncB){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("B");
        }
    }

    public void printC(){
        synchronized (syncC){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("C");
        }
    }

}
