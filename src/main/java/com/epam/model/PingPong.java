package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {
    private static final Logger log = LogManager.getLogger();
    private static Object sync = new Object();

    public static class Bob extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (sync) {
                    System.out.println("Ping");
                    sync.notify();
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Summer extends Thread {
        @Override
        public void run() {

            while (true) {
                synchronized (sync) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Pong");
                }
            }
        }
    }

}





