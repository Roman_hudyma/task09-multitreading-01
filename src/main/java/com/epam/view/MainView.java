package com.epam.view;

import com.epam.controller.*;
import com.epam.model.PingPong;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MainView {
    private static final Logger log = LogManager.getLogger();
    private Scanner scanner = new Scanner(System.in);
    private String menu ="1 - play ping-pong!\n" +
            "2 - start Fibonacci with thread\n" +
            "3 - start Fibonacci with callable!\n" +
            "4 - start sleeping!\n" +
            "5 - run one object at the same!\n"+
            "6 - run three object at the same!\n"+
            "7 - run pipe program!\n"+
            "8 - QUIT!";
    public void run(){
        int choose;

        do {
            System.out.println(menu);
            choose = scanner.nextInt();
            switch (choose) {
                case 1:
                    new PingPong.Bob().start();
                    new PingPong.Summer().start();
                    break;
                case 2:
                    new ThreadFibonacci().run();
                    break;
                case 3:
                    new CallableFibonacci().run();
                    break;
                case 4:
                    new Sleeper().run();
                    break;
                case 5:
                    new Sync().run();
                    break;
                case 6:
                    new MultiSync().run();
                    break;
                case 7:
                    new Pipe().run();
                    break;
            }
        } while (!(8 == choose));
    }
    }

